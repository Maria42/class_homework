/*
 * person.h
 *
 *  Created on: Mar 22, 2020
 *      Author: maria
 */

/*
 *
 * person.h file
 *
 *  Created on: 21/03/2020
 *      Author: maria
 */

#ifndef SRC_PERSON_H_
#define SRC_PERSON_H_

#include <sys/queue.h>

#define DELIMITER ";"
#define STRICT


enum PersonState {
	Married, Divorced, Single, Widow
};



enum Ordering {
	ascending, descending
};


struct Person{
	char *name;
	enum PersonState state;
	int age;
	char *died;
	TAILQ_ENTRY(Person) next;
};



int person_init();


int person_free();


int person_load_from_file(char *fileName);


int person_add(struct Person *p);


struct Person *person_remove_head();


void person_print_a_person(struct Person* p);


void person_print_all();


struct Person *person_find_by_name(char *name, struct Person* elm);


struct Person *person_first();


struct Person *person_last();


struct Person *person_find_by_age(int age, struct Person* elm);


void person_bubble_sort(enum Ordering ordering, int (*comparer(const struct Person *, const struct Person*)));


int person_compare_by_name(const struct Person *a, const struct Person *b);


int person_compare_by_age(const struct Person *a, const struct Person *b);


int person_compare_by_state(const struct Person *a, const struct Person *b);


int person_compare_by_died(const struct Person *a, const struct Person *b);

#endif
